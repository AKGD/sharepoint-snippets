//Populate people picker function
function populatePeoplePicker(fieldInternalName, userEmail) {
    ExecuteOrDelayUntilScriptLoaded(function () {
        setTimeout(function () {
            var ppDiv = $('[id^="' + fieldInternalName + '_"]');
            var ppEditor = ppDiv.find('[id^="' + fieldInternalName + '_"]');
            var spPP = SPClientPeoplePicker.SPClientPeoplePickerDict[ppDiv[0].id];
            ppEditor.val(userEmail);
            spPP.AddUnresolvedUserFromEditor(true);
        }, 0);
    }, 'clientpeoplepicker.js');
}